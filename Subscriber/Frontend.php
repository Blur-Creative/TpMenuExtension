<?php

namespace TpMenuExtension\Subscriber;

use Enlight\Event\SubscriberInterface;
use Shopware\Components\Model\ModelManager;
use Shopware\Models\Category\Repository;
use Shopware\Components\Plugin\CachedConfigReader;

use TpMenuExtension\Service\Category AS TpCategoryService;

class Frontend implements SubscriberInterface {

    /**
     * @var Shopware\Components\Model\ModelManager
     */
    public $manager;

    /**
     * @var Shopware\Models\Category\Repository
     */
    public $categoryRepository;

    /**
     * @var Shopware\Models\Category\Repository
     */
    public $shopRepository;

    /**
     * @var int
     */
    public $customerGroupId;

    /**
     * @var array
     */
    public $config;

    /**
     * @var TpCategoryService
    **/
    public $tpMenuExtensionCategory;

    /**
     * @var int
     */
    public $subshopId;

    /**
     * @var int
     */
    public $subshopEntity;

    public function __construct(
        CachedConfigReader $config,
        TpCategoryService $tpMenuExtensionCategory
    )
    {
        $this->config = $config->getByPluginName('TpMenuExtension');
        $this->tpMenuExtensionCategory = $tpMenuExtensionCategory;
        $this->manager = Shopware()->Container()->get(\Shopware\Components\Model\ModelManager::class);
        $this->shopRepository = $this->manager->getRepository(\Shopware\Models\Shop\Shop::class);
        $this->categoryRepository = $this->manager->getRepository(\Shopware\Models\Category\Category::class);
        $this->customerGroupId = (int) (Shopware()->Modules()->System()->sUSERGROUPDATA['id'] ?? 0);
    }

    public static function getSubscribedEvents()
    {
        return [
            'Enlight_Controller_Action_PostDispatchSecure_Frontend' => 'onFrontend',
            'Legacy_Struct_Converter_Convert_Category' => 'categoryStructConverter'
        ];
    }

    public function onFrontend( \Enlight_Controller_ActionEventArgs $args )
    {
        $view = $args->getSubject()->View();
        $view->assign( 'tpShopCategories', $this->tpMenuExtensionCategory->getShopCategories() );

        $advancedMenuTree = $view->getAssign( 'sAdvancedMenu' );

        $filterKey = key(
            array_filter(
                $advancedMenuTree,
                function( $value ) {
                    return $value[ 'id' ] == $this->config[ 'insertCategory' ];
                }
            ) 
        );

        if ( $this->config[ 'insertPosition' ] ) {
            $filterKey = $filterKey + 1;
        }

        array_splice( 
            $advancedMenuTree, 
            $filterKey, 
            0, 
            array_filter(
                $this->tpMenuExtensionCategory->getShopCategories(),
                function( $value ) {
                    return in_array( $value[ 'id' ], $this->config[ 'subshopCategories' ]  );
                }
            )
        );

        $view->assign( 'sAdvancedMenu', $advancedMenuTree );
    }

    public function categoryStructConverter( $args )
    {
        #dd( $args );
    }
}