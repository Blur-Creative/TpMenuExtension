<?php 

namespace TpMenuExtension\Service;

use Shopware\Components\Plugin\CachedConfigReader;
use Shopware\Components\Model\ModelManager;
use Shopware\Models\Shop\Shop;
use Shopware\Models\Shop\DetachedShop;
use Shopware\Bundle\AttributeBundle\Service\DataLoader;

class Category {

    /**
     * @var string
     */
    protected $pluginName;

    /**
     * @var array
     */
    protected $config;

    /**
     * @var ModelManager
     */
    protected $modelManager;

    /**
     * @var \Shopware_Proxies_ShopwareModelsShopRepositoryProxy
     */
    protected $shopRepository;

    /**
     * @var \Shopware_Proxies_ShopwareModelsCategoryRepositoryProxy
     */
    protected $categoryRepository;

    /**
     * @var int
     */
    protected $customerGroupId;

    /**
     * @var int
     */
    protected $subshopId;

    /**
     * @var DetachedShop
     */
    protected $subshopEntity;

    /**
     * @var int
     */
    protected $subshopCategoryId;

    /**
     * @var DataLoader
     */
    protected $attributeDataLoader;

    public function __construct(
        String $pluginName,
        CachedConfigReader $config,
        ModelManager $modelManager,
        DataLoader $attributeDataLoader
    )
    {
        $this->pluginName = $pluginName;
        $this->config = $config->getByPluginName( $this->pluginName );
        $this->modelManager = $modelManager;
        $this->attributeDataLoader = $attributeDataLoader;

        $this->shopRepository = $this->modelManager->getRepository( \Shopware\Models\Shop\Shop::class );
        $this->categoryRepository = $this->modelManager->getRepository( \Shopware\Models\Category\Category::class );
        $this->customerGroupId = (int) (Shopware()->Modules()->System()->sUSERGROUPDATA['id'] ?? 0);
        $this->subshopId = $this->config[ 'subshopID' ];
        $this->subshopEntity = $this->shopRepository->getById( $this->subshopId );
        $this->subshopCategoryId = $this->subshopEntity->getCategory()->getId();
    }

    public function getShopCategories()
    {
        if ( !empty( $this->getSubshopCategoryId() ) and is_int( $this->getSubshopCategoryId() ) ) {

            $categories = $this->getCategoryTree( 
                $this->getSubshopCategoryId(), 
                null, 
                $this->config[ 'subshopID' ] 
            );

            if ( !empty( $categories ) and !empty( $this->config[ 'subshopID' ]  ) ) {
                return $categories;
            } else {
                return null;
            }
        }
    }

    public function getCategoryTree( 
        ?Int $masterId, 
        ?Int $depth = null,
        ?Int $shopId = null
    ): ?array
    {
        $categories = $this->categoryRepository->getActiveChildrenTree(
            $masterId, 
            $this->customerGroupId, 
            $depth, 
            $shopId
        );
        
        return $this->mapCategories( $categories );
    }

    public function getCategoryAttributes(
        int $categoryId
    )
    {
        return $this->attributeDataLoader->load( 's_categories_attributes', $categoryId );
    }

    protected function mapCategories( $category )
    {
        return array_map (
            function( $value ) {

                /**
                 * add necessary properties to category tree
                 */
                $value[ 'link' ] = $this->buildCategoryUrl( $value[ 'id' ] );
                $value[ 'attributes' ] = $this->getCategoryAttributes( $value[ 'id' ] );

                /**
                 * loop mapping for subcategories
                 */
                $value[ 'sub' ] = $this->mapCategories( $value[ 'sub' ] );

                return $value;

            }, $category
        );
    }

    protected function buildCategoryUrl( 
        int $categoryId 
    )
    {
        $host = $this->getSubshopEntity()->getHost();
        $basePath = $this->getSubshopEntity()->getBasePath();
        $baseUrl = $this->getSubshopEntity()->getBaseUrl();
        $isSecured = $this->getSubshopEntity()->getSecure() ? true : false;

        $url = null;

        if ( $isSecured === true ) {
            $url .= "https://";
        } elseif ( $isSecured === false ) {
            $url .= "http://";
        } else {
            $url .= "//";
        }

        $url .= $host;
        $url .= $baseUrl;
        $url .= "?sViewport=cat&sCategory=";
        $url .= $categoryId;

        return $url;
    }

    protected function getSubshopId(): Int
    {
        return $this->subshopId;
    }

    protected function getSubshopEntity(): DetachedShop
    {
        return $this->subshopEntity;
    }


    protected function getSubshopCategoryId(): Int
    {
        return $this->subshopCategoryId;
    }
}